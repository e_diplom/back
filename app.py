import os
import threading
import time
from datetime import datetime, timedelta
import requests
from flask import Flask, jsonify, make_response, request, abort
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

database_name = os.environ['DBNAME'] 
username = os.environ['DBUSERNAME']
password = os.environ['DBPASSWORD']
endpoint = os.environ['DBENDPOINT']

frost_client_id = os.environ['FROSTID']
frost_endpoint = os.environ['FROSTURL']

db = SQLAlchemy()

class Station(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    station_id = db.Column(db.String(), index=True, unique=True)
    name = db.Column(db.String())
    dataframes = db.relationship('Dataframe', backref='station', lazy='dynamic')
 
    def __init__(self, station_id, name):
        self.station_id = station_id
        self.name = name

class Dataframe(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    dataframe = db.Column(db.String(), index=True, unique=True)
    station_id = db.Column(db.String(), db.ForeignKey('station.station_id'))
    winds = db.relationship('Wind', backref='df', lazy='dynamic')

class Wind(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    time = db.Column(db.DateTime())
    wind = db.Column(db.String())
    dataframe = db.Column(db.String(), db.ForeignKey('dataframe.dataframe'))

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://{0}:{1}@{2}/{3}'.format(username,password,endpoint,database_name)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

def add_station_func(stat_id, stat_name):
    new_station = Station(station_id=stat_id, name=stat_name)
    db.session.add(new_station)
    db.session.commit()
    return 1

def add_dataframe_func(df, stat_id):
    new_df = Dataframe(dataframe=df, station_id=stat_id)
    db.session.add(new_df)
    db.session.commit()
    return 1

def add_wind_func(df, wind_val, wind_time):
    new_wind = Wind(dataframe=df, wind=wind_val, time=wind_time)
    db.session.add(new_wind)
    db.session.commit()
    return 1

def get_requests(url, params, auth):
    r = requests.get(url, params, auth=auth)
    json_response = r.json()
    if r.status_code == 200:
        data = json_response['data']
        print('Data retrieved from url')
    else:
        print('Error! Returned status code %s' % r.status_code)
        print('Message: %s' % json_response['error']['message'])
        print('Reason: %s' % json_response['error']['reason'])
    return data

@app.before_first_request
def fill_db():
    print('DB FILLER')
    if Station.query.first():
        print('DATABASE FILLED')
    else:
        print('DATABASE EMPTY')
        #
        # GET FROST STATIONS DATA
        #
        parameters = {
            'country': 'Norge',
            'elements': 'wind_from_direction',
            'fields': 'id,name',
        }
        stations = get_requests('{0}/sources/v0.jsonld'.format(frost_endpoint), parameters, auth=(frost_client_id,''))
        stations = stations[0:10]
        #
        # GET FROST DATAFRAMES
        #
        for s in range(len(stations)):
            add_station_func(stations[s]['id'], stations[s]['name'])
            #
            parameters = {
                'sources': stations[s]['id'],
                'elements': 'wind_from_direction',
                'fields': 'validFrom',
            }
            dataframes_unsorted = get_requests('{0}/observations/availableTimeSeries/v0.jsonld'.format(frost_endpoint), parameters, auth=(frost_client_id,''))
            dataframes = sorted(dataframes_unsorted, key = lambda i: i['validFrom'],reverse=True)
            #
            for d in range(len(dataframes)):
                add_dataframe_func(dataframes[d]['validFrom'], stations[s]['id'])
                #
                # 
                #
                parameters = {}
                utc_dt = datetime.strptime(dataframes[d]['validFrom'], '%Y-%m-%dT%H:%M:%S.%fZ')
                utc_dt_next = utc_dt + timedelta(days=1)
                utc_dt_short = utc_dt.strftime('%Y-%m-%d')
                utc_dt_next_short = utc_dt_next.strftime('%Y-%m-%d')
                parameters = {
                    'sources': stations[s]['id'],
                    'elements': 'wind_from_direction',
                    'referencetime': '{0}/{1}'.format(utc_dt_short, utc_dt_next_short),
                    'fields': 'value, referenceTime',
                }
                winds = get_requests('{0}/observations/v0.jsonld'.format(frost_endpoint), parameters, auth=(frost_client_id,''))
                for w in range(len(winds)):
                    add_wind_func(dataframes[d]['validFrom'], winds[w]['observations'][0]['value'], winds[w]['referenceTime'])
    return 1


# GET ALL STATIONS
@app.route('/api/stations', methods=['GET'])
def get_stations():
    cols = ['station_id', 'name']
    stations = Station.query.limit(10).all()
    result = [{col: getattr(d, col) for col in cols} for d in stations]
    return jsonify(stations=result)


# GET SPECIFIC STATION
@app.route('/api/station/<req_id>', methods=['GET'])
def get_station(req_id):
    station = Station.query.filter_by(station_id=req_id).first()
    print(station)
    result = {
            'station_id': station.station_id,
            'name': station.name
    }
    return jsonify(station=result)

# ADD STATION
@app.route('/api/station', methods=['POST'])
def add_station():
    if not request.json:
        abort(400)
    if not 'station_id' in request.json:
        abort(400)
    if not 'name' in request.json:
        request.json['name'] = ''

    add_station_func(request.json['station_id'], request.json['name'])

    return jsonify({'success': 1}), 201

# ADD DATAFRAME
@app.route('/api/dataframe', methods=['POST'])
def add_dataframe():
    if not request.json:
        abort(400)
    if not 'station_id' in request.json:
        abort(400)
    if not 'dataframe' in request.json:
        abort(400)

    add_dataframe_func(request.json['dataframe'], request.json['station_id'])

    return jsonify({'success': 1}), 201


# ADD WIND
@app.route('/api/wind', methods=['POST'])
def add_wind():
    if not request.json:
        abort(400)
    if not 'dataframe' in request.json:
        abort(400)
    if not 'time' in request.json:
        abort(400)
    if not 'wind' in request.json:
        abort(400)

    new_wind = Wind(dataframe=request.json['dataframe'], wind=request.json['wind'], time=request.json['time'])
    db.session.add(new_wind)
    db.session.commit()

    return jsonify({'success': 1}), 201

# GET WINDS
@app.route('/api/wind/<stat_id>/<df>', methods=['GET'])
def get_wind(stat_id, df):
    cols = ['id', 'time', 'wind']
    winds = Wind.query.filter_by(dataframe=df).order_by(Wind.time)
    result = [{col: getattr(d, col) for col in cols} for d in winds]
    return jsonify(winds=result)

# GET WIND BY ID
@app.route('/api/wind/<stat_id>/<df>/<wind_id>', methods=['GET'])
def get_wind_by_id(stat_id, df, wind_id):
    cols = ['id', 'time', 'wind']
    wind = Wind.query.filter_by(id=wind_id)
    result = [{col: getattr(d, col) for col in cols} for d in wind]
    return jsonify(wind=result)

# GET DATAFRAME
@app.route('/api/wind/<stat_id>', methods=['GET'])
def get_dataframes(stat_id):
    cols = ['dataframe']
    dataframes = Dataframe.query.filter_by(station_id=stat_id)
    result = [{col: getattr(d, col) for col in cols} for d in dataframes]
    return jsonify(dataframes=result)

# EDIT STATION
@app.route('/api/station/<station_id>', methods=['PUT'])
def edit_station(station_id):
    if not request.json:
        abort(400)
    if not 'station_id' in request.json:
        abort(400)
    if not 'name' in request.json:
        request.json['name'] = ''

    new_station = Station.query.filter_by(station_id=station_id).first()
    new_station.station_id = request.json['station_id']
    new_station.name = request.json['name']
    db.session.add(new_station)
    db.session.commit()

    return jsonify({'success': 1}), 201

# DELETE STATION
@app.route('/api/station/<station_id>', methods=['DELETE'])
def delete_station(station_id):
    if not request.json:
        abort(400)

    new_station = Station.query.filter_by(station_id=station_id).first()
    db.session.delete(new_station)
    db.session.commit()

    return jsonify({'success': 1}), 201

# EDIT WIND
@app.route('/api/wind/<station_id>/<dataframe>/<wind_id>', methods=['PUT'])
def edit_wind(station_id, dataframe, wind_id):
    if not request.json:
        abort(400)
    if not 'time' in request.json:
        abort(400)
    if not 'wind' in request.json:
        abort(400)

    new_wind = Wind.query.filter_by(id=wind_id).first()
    new_wind.time = request.json['time']
    new_wind.wind = request.json['wind']
    db.session.add(new_wind)
    db.session.commit()

    return jsonify({'success': 1}), 201

# DELETE WIND
@app.route('/api/wind/<station_id>/<dataframe>/<wind_id>', methods=['DELETE'])
def delete_wind(station_id, dataframe, wind_id):
    if not request.json:
        abort(400)

    wind = Wind.query.filter_by(id=wind_id).first()
    db.session.delete(wind)
    db.session.commit()

    return jsonify({'success': 1}), 201

# ERROR HANDLER
@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

def start_db_checker():
    def start_loop():
        not_started = True
        while not_started:
            print('In start loop')
            try:
                headers = {'Content-type': 'application/json'}
                r = requests.get('http://127.0.0.1:8000/api/stations', headers=headers)
                if r.status_code == 200:
                    print('Server started, quiting start_loop')
                    not_started = False
                print(r.status_code)
            except:
                print('Server not yet started')
            time.sleep(2)

    print('Started runner')
    thread = threading.Thread(target=start_loop)
    thread.start()



if __name__ == '__main__':
    db.init_app(app)
    with app.app_context():
        db.create_all()
    migrate = Migrate(app, db)
    start_db_checker()
    app.run(debug=True, host='0.0.0.0', port='8000')
